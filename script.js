const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

//Display 1-20 - div1

function oneThroughTwenty() {
    const numbers = []
    for (let counter = 1; counter <= 20; counter++) {
        numbers.push(counter)
        }
    return numbers;
}
    let div1Text = document.createElement('div');
    div1Text.innerHTML = oneThroughTwenty();
    let div1 = document.getElementById('div1');
    div1.appendChild(div1Text);
    


//Display evens to 20 - div2
function evensToTwenty() {
    const numbers = []
    for (let counter = 2; counter <= 20; counter+=2) {
        numbers.push(counter)
    }
    return numbers;
}
let div2Text = document.createElement('div');
div2Text.innerHTML = evensToTwenty();
let div2 = document.getElementById('div2');
div2.appendChild(div2Text);



// //Display odds to 20 - div3
function oddsToTwenty() {
    const numbers = []
    for (let counter = 1; counter <= 20; counter+=2) {
        numbers.push(counter)
    }
    return numbers;
}
let div3Text = document.createElement('div');
div3Text.innerHTML = oddsToTwenty();
let div3 = document.getElementById('div3');
div3.appendChild(div3Text);


//Display multiples of five - div4
function multiplesOfFive() {
    const numbers = []
    for (let counter = 5; counter <=100; counter+=5) {
        numbers.push(counter)
    }
    return numbers;
}
let div4Text = document.createElement('div');
div4Text.innerHTML = multiplesOfFive();
let div4 = document.getElementById('div4');
div4.appendChild(div4Text);

//Display square numbers to 100 - div5
function squareNumbers() {
    const numbers = []
    for (let counter=1; counter<=10; counter++) {
        numbers.push(counter**2)
    }
    return numbers;
}
let div5Text = document.createElement('div');
div5Text.innerHTML = squareNumbers();
let div5 = document.getElementById('div5');
div5.appendChild(div5Text);

//Display backwards to 20 - div6
function countingBackwards() {
    const numbers = []
    for (let counter=20; counter >=1; counter-=1){
        numbers.push(counter)
    }
    return numbers;
}
let div6Text = document.createElement('div');
div6Text.innerHTML = countingBackwards();
let div6 = document.getElementById('div6');
div6.appendChild(div6Text);

//Display evens from 20-0 - div7
function evenNumbersBackwards() {
    const numbers = []
    for (let counter=20; counter>=2; counter-=2){
        numbers.push(counter)
    }
    return numbers;
}
let div7Text = document.createElement('div');
div7Text.innerHTML = evenNumbersBackwards();
let div7 = document.getElementById('div7');
div7.appendChild(div7Text);

//Display odds from 19-1 - div8
function oddNumbersBackwards() {
    const numbers = []
    for (let counter=19; counter>=1; counter-=2){
        numbers.push(counter)
    }
    return numbers;
}
let div8Text = document.createElement('div');
div8Text.innerHTML = oddNumbersBackwards();
let div8 = document.getElementById('div8');
div8.appendChild(div8Text);

//Display multiples of five backward - div9
function multiplesOfFiveBackwards() {
    const numbers = []
    for (let counter=100; counter>=5; counter-=5){
        numbers.push(counter)
    }
    return numbers;
}
let div9Text = document.createElement('div');
div9Text.innerHTML = multiplesOfFiveBackwards();
let div9 = document.getElementById('div9');
div9.appendChild(div9Text);

//Display squared numbers backwards - div10
function squareNumbersBackwards() {
    const numbers = []
    for (let counter=10; counter>=1; counter--){
        numbers.push(counter**2)
    }
    return numbers;
}
let div10Text = document.createElement('div');
div10Text.innerHTML = squareNumbersBackwards();
let div10 = document.getElementById('div10');
div10.appendChild(div10Text);

//display the Sample Array - div11
let div11 = document.getElementById('div11');
let numberElevenText = document.createTextNode(sampleArray)
div11.appendChild(numberElevenText);

// display the evens of the Sample Array - div12
function evensOfSampleArray () {
    const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let evensArray = []
    for (let i=0; i<sampleArray.length; i++) {
        if (sampleArray[i] % 2 === 0) {
            evensArray.push(sampleArray[i]);
        }
    }
    return evensArray;
}
let div12Text = document.createElement('div');
div12Text.innerHTML = evensOfSampleArray();
let div12 = document.getElementById('div12');
div12.appendChild(div12Text);

//display the odds of Sample Array - div13
function oddsOfSampleArray () {
    const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let oddsArray = []
    for (let i=0; i<sampleArray.length; i++) {
        if (sampleArray[i] % 2 !== 0) {
            oddsArray.push(sampleArray[i]);
        }
    }
    return oddsArray;
}
let div13Text = document.createElement('div');
div13Text.innerHTML = oddsOfSampleArray();
let div13 = document.getElementById('div13');
div13.appendChild(div13Text);

//Display the square of each number in Sample Array - div14
function squareNumbersOfArray() {
    const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    const numbers = []
    for (let i=0; i<sampleArray.length; i++) {
        numbers.push(sampleArray[i]**2)
    }
    return numbers;
}
let div14Text = document.createElement('div');
div14Text.innerHTML = squareNumbersOfArray();
let div14 = document.getElementById('div14');
div14.appendChild(div14Text);

//Display the sum of numbers to 20 - div15
function sumToTwenty() {
    let total = 0
    for (let i=0; i<21; i++) {
        total = total + i
    }
    return total;
}
let div15Text = document.createElement('div');
div15Text.innerHTML = sumToTwenty();
let div15 = document.getElementById('div15');
div15.appendChild(div15Text);

//Display the sum of the Sample Array -div16
function sumOfSampleArray () {
    const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let total = 0
    for (let i=0; i<sampleArray.length; i++) {
        total = total + sampleArray[i]
    }
    return total;
}
let div16Text = document.createElement('div');
div16Text.innerHTML = sumOfSampleArray();
let div16 = document.getElementById('div16');
div16.appendChild(div16Text);

//Display the smallest element in the Sample Array -div17
function smallestNumberInSampleArray () {
    const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let smallestNumber = sampleArray[0];
    for (let i=0; i<=sampleArray.length; i++) {
        if (smallestNumber > sampleArray[i]) {
            smallestNumber = sampleArray[i];
        }
    }
    return smallestNumber
}
let div17Text = document.createElement('div');
div17Text.innerHTML = smallestNumberInSampleArray();
let div17 = document.getElementById('div17');
div17.appendChild(div17Text);

// //Display the largest element in the Sample Array -div18

function largestNumberInSampleArray () {
    const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
    let largestNumber = sampleArray [0];
    for (let i=1; i<sampleArray.length; i++) {
        if (largestNumber < sampleArray[i]) {
            largestNumber = sampleArray[i];
        }
    }
    return largestNumber;
}
let div18Text = document.createElement('div');
div18Text.innerHTML = largestNumberInSampleArray();
let div18 = document.getElementById('div18');
div18.appendChild(div18Text);

















// // 